package Model;

public class Store {

	private int storeId;
	private int orderId;
	private String country;
	
	
	public Store(int storeId, int orderId, String country){
		super();
		this.storeId = storeId;
		this.orderId = orderId;
		this.country = country;
	}
	
	public Store(int orderId, String country){
		super();
		this.orderId = orderId;
		this.country = country;
	}
	
	public int getStoreId(){
		return storeId;
	}
	
	public void setStoreId(int storeId){
		this.storeId = storeId;
	}
	
	public int getOrderId(){
		return orderId;
	}
	
	public void setOrderId(int orderId){
		this.orderId = orderId;
	}
	
	public String getCountry(){
		return country;
	}
	
	public void setCountry(String country){
		this.country = country;
	}
	
	public String toString() {
		return "Store [id=" + storeId + ", order ID=" + orderId +  ", country=" + country + "]";
	}
}

