package Model;

public class Order {
	
	private int orderId;
	private int clientId;
	private int productId;
	private int quantity;
	
	
	public Order(int id, int cId, int pId, int q){
		super();
		orderId = id;
		clientId = cId;
		productId = pId;
		quantity = q;
	}
	
	public Order(int cId, int pId, int q){
		super();
		clientId = cId;
		productId = pId;
		quantity = q;
	}
	
	public int getOrderId(){
		return orderId;
	}
	
	public void setOrderId(int orderId){
		this.orderId = orderId;
	}
	
	public int getClientId(){
		return clientId;
	}
	
	public void setClientId(int clientId){
		this.clientId = clientId;
	}
	
	public int getProductId(){
		return productId;
	}
	
	public void setProductId(int productId){
		this.productId = productId;
	}
	
	public int getQuantity(){
		return quantity;
	}
	
	public void setQuantity(int quantity){
		this.quantity = quantity;
	}
	
	public String toString() {
		return "Order [id=" + productId + ", client ID=" + clientId + ", product ID=" + productId + ", quantity=" + quantity + "]";
	}
}
