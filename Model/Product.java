package Model;

public class Product {
	
	private int productId;
	private String name;
	private String type;
	private int quantity;
	
	
	public Product(int productId, String name, String type, int quantity) {
		super();
		this.name = name;
		this.productId = productId;
		this.type = type;
		this.quantity = quantity;
	}
	
	public Product(String name, String type, int quantity) {
		super();
		this.name = name;
		this.type = type;
		this.quantity = quantity;
	}
	
	public int getId() {
		return productId;
	}

	public void setId(int id) {
		this.productId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	
	public String toString() {
		return "Product [id=" + productId + ", name=" + name + ", type=" + type + ", quantity=" + quantity + "]";
	}
}
