package Model;

public class Client {
	private int clientId;
	private String name;
	private int phone;
	private String address;
	private String email;
	
	
	public Client(String name, int phone, String address, String email) {
		super();
		this.name = name;
		this.address = address;
		this.email = email;
		this.phone = phone;
	}
	
	public Client(int clientId, String name, int phone, String address, String email) {
		super();
		this.clientId = clientId;
		this.name = name;
		this.address = address;
		this.email = email;
		this.phone = phone;
	}
	
	public int getId() {
		return clientId;
	}

	public void setId(int id) {
		this.clientId = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getPhone() {
		return phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}
	
	public String toString() {
		return "Client [id=" + clientId + ", name=" + name + ", address=" + address + ", phone=" + phone + ", email=" + email + "]";
	}
}
