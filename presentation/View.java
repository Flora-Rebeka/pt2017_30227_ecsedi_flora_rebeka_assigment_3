package presentation;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

import Model.Client;
import connection.ConnectionFactory;
import dao.ClientDao;

import java.awt.List;
import java.awt.ScrollPane;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

public class View extends SecondFrame{
	
	private JPanel panel=new JPanel();
	
	
	private JLabel clientLabel=new JLabel("Client");
	private JLabel productLabel=new JLabel("Product");
	private JLabel orderLabel=new JLabel("Order");
	
	private JButton findClient=new JButton("Find");
	private JButton insertClient=new JButton("Insert");	
	private JButton updateClient=new JButton("Update");
	private JButton deleteClient=new JButton("Delete");
	private JButton showClientTable=new JButton("Show");
	
	private JButton findProduct=new JButton("Find");
	private JButton insertProduct=new JButton("Insert");	
	private JButton updateProduct=new JButton("Update");
	private JButton deleteProduct=new JButton("Delete");
	private JButton showProductTable=new JButton("Table");
	
	private JButton createOrder=new JButton("Create");
	
	//buttons for do insert,find,update,etc...
	private JButton findClientT=new JButton("Submit");
	private JButton insertClientT=new JButton("Submit");	
	private JButton updateClientT=new JButton("Submit");
	private JButton deleteClientT=new JButton("Submit");
	private JButton showClientTableT=new JButton("Submit");
	
	private JButton findProductT=new JButton("Submit");
	private JButton insertProductT=new JButton("Submit");	
	private JButton updateProductT=new JButton("Submit");
	private JButton deleteProductT=new JButton("Submit");
	private JButton showProductTableT=new JButton("Submit");
	
	private JButton createOrderT=new JButton("Create");
	
	
	//for client operations
	private JLabel l=new JLabel("Client Id");
	private JTextField f=new JTextField();
	
	private JLabel l1=new JLabel("Client name");
	private JTextField f1=new JTextField();
	
	private JLabel l2=new JLabel("Client phone");
	private JTextField f2=new JTextField();
	
	private JLabel l3=new JLabel("Client address");
	private JTextField f3=new JTextField();
	
	private JLabel l4=new JLabel("Client email");
	private JTextField f4=new JTextField();
	
	
	//for product operations
	private JLabel ll=new JLabel("Product Id");
	private JTextField ff=new JTextField();
	
	private JLabel ll1=new JLabel("Product name");
	private JTextField ff1=new JTextField();
	
	private JLabel ll2=new JLabel("Product type");
	private JTextField ff2=new JTextField();
	
	private JLabel ll3=new JLabel("Product quantity");
	private JTextField ff3=new JTextField();
	
	
	//for order
	
	private JLabel clientIdLabel = new JLabel("Client Id");
	private JTextField clientIdTextField = new JTextField();
	private JLabel productIdLabel = new JLabel("Product Id");
	private JTextField productIdTextField = new JTextField();
	private JLabel quantityLabel = new JLabel("Quantity");
	private JTextField quantityTextField = new JTextField();
	
	
	public View(){
		add(panel);
		panel.setLayout(null);
		setComponents();
		addComponents();
		jFrameSetup();
	}
	private void jFrameSetup(){
		setSize(400,350);
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
	public void setComponents(){
		clientLabel.setBounds(20,20,100,20);
		productLabel.setBounds(130,20,100,20);
		orderLabel.setBounds(260,20,100,20);
		
		findClient.setBounds(20,50,100,25);
		insertClient.setBounds(20,80,100,25);
		updateClient.setBounds(20,110,100,25);
		deleteClient.setBounds(20,140,100,25);
		showClientTable.setBounds(20,170,100,25);
		
		findProduct.setBounds(130,50,100,25);
		insertProduct.setBounds(130,80,100,25);
		updateProduct.setBounds(130,110,100,25);
		deleteProduct.setBounds(130,140,100,25);
		showProductTable.setBounds(130,170,100,25);
		
		createOrder.setBounds(260,50,100,25);
	}
	public void addComponents(){
		panel.add(clientLabel);//Labels
		panel.add(productLabel);
		panel.add(orderLabel);
		
		panel.add(findClient);
		panel.add(insertClient);
		panel.add(updateClient);
		panel.add(deleteClient);
		panel.add(showClientTable);
		
		panel.add(findProduct);
		panel.add(insertProduct);
		panel.add(updateProduct);
		panel.add(deleteProduct);
		panel.add(showProductTable);
		
		panel.add(createOrder);
	}
	
	 public void setFindClientListener(ActionListener a){
		findClient.addActionListener(a);
	 }
	 public void setInsertClientListener(ActionListener a){
		 insertClient.addActionListener(a);
	 }
	 public void setUpdateClientListener(ActionListener a){
		 updateClient.addActionListener(a);
	 }
	 public void setDeleteClientListener(ActionListener a){
		 deleteClient.addActionListener(a);
	 }
	 public void setShowClientListener(ActionListener a){
		 showClientTable.addActionListener(a);
	 }
	 public void setFindProductListener(ActionListener a){
		findProduct.addActionListener(a);
	 }
	 public void setInsertProductListener(ActionListener a){
		 insertProduct.addActionListener(a);
	 }
	 public void setUpdateProductListener(ActionListener a){
		 updateProduct.addActionListener(a);
	 }
	 public void setDeleteProductListener(ActionListener a){
		 deleteProduct.addActionListener(a);
	 }
	 public void setShowProductListener(ActionListener a){
		 showProductTable.addActionListener(a);
	 }
	 public void setCreateOrderListener(ActionListener a){
		 createOrder.addActionListener(a);
	 }
	 
	 
	 //submit button listeners
	 public void setFindClientTListener(ActionListener a){
			findClientT.addActionListener(a);
		 }
		 public void setInsertClientTListener(ActionListener a){
			 insertClientT.addActionListener(a);
		 }
		 public void setUpdateClientTListener(ActionListener a){
			 updateClientT.addActionListener(a);
		 }
		 public void setDeleteClientTListener(ActionListener a){
			 deleteClientT.addActionListener(a);
		 }
		 public void setFindProductTListener(ActionListener a){
			findProductT.addActionListener(a);
		 }
		 public void setInsertProductTListener(ActionListener a){
			 insertProductT.addActionListener(a);
		 }
		 public void setUpdateProductTListener(ActionListener a){
			 updateProductT.addActionListener(a);
		 }
		 public void setDeleteProductTListener(ActionListener a){
			 deleteProductT.addActionListener(a);
		 }
		 public void setCreateOrderTListener(ActionListener a){
			 createOrderT.addActionListener(a);
		 }
	 //client components to appear in the frame
	 public void findClientFrame(){
		 SecondFrame f = new SecondFrame();
		 setFindClientComponents(f.panel);
		 findClientT.setBounds(140,180,100,20);
		 f.panel.add(findClientT);
	 }
	 public void setFindClientComponents(JPanel p){
		 l.setBounds(140,60,100,20);
		 f.setBounds(140,120,100,20);
		 
		 p.add(l);
		 p.add(f);
	 }
	 public void insertClientFrame(){
		 SecondFrame f = new SecondFrame();
		 setInsertClientComponents(f.panel);
		 insertClientT.setBounds(140, 300, 100, 20);
		 f.panel.add(insertClientT);
	 }
	 public void setInsertClientComponents(JPanel p){ 
		 l1.setBounds(140,20,100,20);
		 f1.setBounds(140,50,100,20);
		 l2.setBounds(140,80,100,20);
		 f2.setBounds(140,120,100,20);
		 l3.setBounds(140,150,100,20);
		 f3.setBounds(140,180,100,20);
		 l4.setBounds(140,210,100,20);
		 f4.setBounds(140,240,100,20);
		 
		 p.add(l1);
		 p.add(f1);
		 p.add(l2);
		 p.add(f2);
		 p.add(l3);
		 p.add(f3);
		 p.add(l4);
		 p.add(f4);
	 }
	 public void updateClientFrame(){
		 SecondFrame f = new SecondFrame();
		 setUpdateClientComponents(f.panel);
		 updateClientT.setBounds(140,370,100,20);
		 f.panel.add(updateClientT);
	 }
	 public void setUpdateClientComponents(JPanel panel){
		 
		 l1.setBounds(140,20,100,20);
		 f1.setBounds(140,50,100,20);
		 l2.setBounds(140,80,100,20);
		 f2.setBounds(140,120,100,20);
		 l3.setBounds(140,150,100,20);
		 f3.setBounds(140,180,100,20);
		 l4.setBounds(140,210,100,20);
		 f4.setBounds(140,240,100,20);
		 l.setBounds(140,270,100,20);
		 f.setBounds(140,300,100,20);
		 
		 panel.add(l);
		 panel.add(f);
		 panel.add(l1);
		 panel.add(f1);
		 panel.add(l2);
		 panel.add(f2);
		 panel.add(l3);
		 panel.add(f3);
		 panel.add(l4);
		 panel.add(f4);
	 }
	 public void deleteClientFrame(){
		 SecondFrame f = new SecondFrame();
		 setDeleteClientComponents(f.panel);
		 deleteClientT.setBounds(140,150,100,20);
		 f.panel.add(deleteClientT);
	 }
	 public void setDeleteClientComponents(JPanel panel){
		 l.setBounds(140,60,100,20);
		 f.setBounds(140,120,100,20);
		 panel.add(l);
		 panel.add(f);
	 }
	 //product components to appear in the frame
	 public void findProductFrame(){
		 SecondFrame f = new SecondFrame();
		 setFindProductComponents(f.panel);
		 findProductT.setBounds(20,80,100,20);
		 f.panel.add(findProductT);
	 }
	 public void setFindProductComponents(JPanel panel){
		 ll.setBounds(20,20,100,20);
		 ff.setBounds(20,50,100,20);
		 panel.add(ll);
		 panel.add(ff);
	 }
	 public void insertProductFrame(){
		 SecondFrame f = new SecondFrame();
		 setInsertProductComponents(f.panel);
		 insertProductT.setBounds(140,300,100,20);
		 f.panel.add(insertProductT);
	 }
	 public void setInsertProductComponents(JPanel panel){
		 
		 ll.setBounds(140,20,100,20);
		 ff.setBounds(140,50,100,20);
		 ll1.setBounds(140,80,100,20);
		 ff1.setBounds(140,120,100,20);
		 ll2.setBounds(140,150,100,20);
		 ff2.setBounds(140,180,100,20);
		 ll3.setBounds(140,210,100,20);
		 ff3.setBounds(140,240,100,20);
		 
		 panel.add(ll1);
		 panel.add(ff1);
		 panel.add(ll2);
		 panel.add(ff2);
		 panel.add(ll3);
		 panel.add(ff3);
	 }
	 public void updateProductFrame(){
		 SecondFrame f = new SecondFrame();
		 setUpdateProductComponents(f.panel);
		 updateProductT.setBounds(140, 300, 100, 20);
		 f.panel.add(updateProductT);
	 }
	 public void setUpdateProductComponents(JPanel panel){
		 
		 ll.setBounds(140,20,100,20);
		 ff.setBounds(140,50,100,20);
		 ll1.setBounds(140,80,100,20);
		 ff1.setBounds(140,120,100,20);
		 ll2.setBounds(140,150,100,20);
		 ff2.setBounds(140,180,100,20);
		 ll3.setBounds(140,210,100,20);
		 ff3.setBounds(140,240,100,20);
		 
		 
		 panel.add(ll);
		 panel.add(ff);
		 panel.add(ll1);
		 panel.add(ff1);
		 panel.add(ll2);
		 panel.add(ff2);
		 panel.add(ll3);
		 panel.add(ff3);
	 }
	 public void deleteProductFrame(){
		 SecondFrame f = new SecondFrame();
		 setDeleteProductComponents(f.panel);
		 deleteProductT.setBounds(140, 85, 100, 20);
		 f.panel.add(deleteProductT);
	 }
	 public void setDeleteProductComponents(JPanel panel){
		 
		 ll.setBounds(140,20,100,20);
		 ff.setBounds(140,50,100,20);
		 
		 panel.add(ll);
		 panel.add(ff);
	 }
	 ////////////////////////////////////////////////
	
	 public void createTable(){
		
				//headers for the table
		        String[] columns = new String[] {
		           "Id", "Name", "Phone", "Email", "Address"
		        };
		         
		        //actual data for the table in a 2d array
		        Object[][] data = new Object[][] {
		            {1, "Jim", "0123456789" , "Email","Cluj"},
		            {2, "Ram", "0123456789", "Email","Oradea" },
		            {3, "Zig", "0123456789", "Email","Timisoara" },
		        };
		        //create table with data
		        JTable table = new JTable(data, columns);
		        
		        SecondFrame f = new SecondFrame();
				setCreateOrderFrameComponents(f.panel);
				//f.panel.add(table);
		        //add the table to the frame
				f.panel.add(new JScrollPane(table));
		        
		       
	 }
	////////////////////////////////////////////////
	
	public void createOrderFrame(){
		 SecondFrame f = new SecondFrame();
		 setCreateOrderFrameComponents(f.panel);
		 createOrderT.setBounds(20, 20, 100, 20);
		 f.panel.add(createOrderT);
	 }
	public void setCreateOrderFrameComponents(JPanel panel){
		clientIdLabel.setBounds(20, 60, 100, 20);
		clientIdTextField.setBounds(20, 100, 100, 20);
		productIdLabel.setBounds(20, 140, 100, 20);
		productIdTextField.setBounds(20, 180, 100, 20);
		quantityLabel.setBounds(20, 220, 100, 20);
		quantityTextField.setBounds(20, 260, 100, 20);
		
		 panel.add(clientIdLabel);
		 panel.add(clientIdTextField);
		 panel.add(productIdLabel);
		 panel.add(productIdTextField);
		 panel.add(quantityLabel);
		 panel.add(quantityTextField);
	}
	
	///////////////////////////////////////////////////
	
	public int getClientId(){
		return Integer.parseInt(f.getText());
	}
	
	public String getClientName(){
		return f1.getText();
	}
	
	public int getClientPhone(){
		return Integer.parseInt(f2.getText());
	}
	
	public String getClientAddress(){
		return f3.getText();
	}
	
	public String getClientEmail(){
		return f4.getText();
	}
	
	public int getProductId(){
		return Integer.parseInt(ff.getText());
	}
	
	public String getProductName(){
		return ff1.getText();
	}
	
	public String getProductType(){
		return ff2.getText();
	}
	
	public int getProductQuantity(){
		return Integer.parseInt(ff3.getText());
	}
	
	public int getClientId2(){
		return Integer.parseInt(clientIdTextField.getText());
	}
	public int getProductId2(){
		return Integer.parseInt(productIdTextField.getText());
	}
	public int getProductQuantity2(){
		return Integer.parseInt(quantityTextField.getText());
	}
}
