package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;

import javax.swing.table.DefaultTableModel;

import Model.Client;
import Model.Order;
import Model.Product;
import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import dao.ClientDao;
import connection.ConnectionFactory;



public class Controler {

	private View view;
	
	public Controler(View v){
		this.view = v;
		
		view.setFindClientListener(new FindClientListener());
		view.setInsertClientListener(new InsertClientListener());
		view.setUpdateClientListener(new UpdateClientListener());
		view.setDeleteClientListener(new DeleteClientListener());
		view.setShowClientListener(new ShowClientListener());
		
		view.setFindProductListener(new FindProductListener());
		view.setInsertProductListener(new InsertProductListener());
		view.setUpdateProductListener(new UpdateProductListener());
		view.setDeleteProductListener(new DeleteProductListener());
		view.setShowProductListener(new ShowProductListener());
		
		view.setCreateOrderListener(new CreateOrderListener());
		
		view.setFindClientTListener(new FindClientTListener());
		view.setInsertClientTListener(new InsertClientTListener());
		view.setUpdateClientTListener(new UpdateClientTListener());
		view.setDeleteClientTListener(new DeleteClientTListener());
		
		view.setFindProductTListener(new FindProductTListener());
		view.setInsertProductTListener(new InsertProductTListener());
		view.setUpdateProductTListener(new UpdateProductTListener());
		view.setDeleteProductTListener(new DeleteProductTListener());
		
		view.setCreateOrderTListener(new CreateOrderTListener());
		
		
		ProductBLL produs = new ProductBLL();
	}
	
	public class FindClientListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.findClientFrame();
		}
	}
	
	public class InsertClientListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.insertClientFrame();
		}
	}
	
	public class UpdateClientListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.updateClientFrame();
		}
	}
	
	public class DeleteClientListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.deleteClientFrame();
		}
	}
	
	public class ShowClientListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			//view.createTable();
			System.out.println("Success!");
		}
	}
	
	public class FindProductListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.findProductFrame();
		}
	}
	
	public class InsertProductListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.insertProductFrame();
		}
	}
	
	public class UpdateProductListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.updateProductFrame();
		}
	}
	
	public class DeleteProductListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.deleteProductFrame();
		}
	}
	
	public class ShowProductListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			//view.createTable();
			System.out.println("Success!");
		}
	}
	
	public class CreateOrderListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.createOrderFrame();
		}
	}
	
	
	//////////////////////////////ascultatori pt butoanele de submit, in caz de succes af. succes!
	
	public class FindClientTListener implements ActionListener{
		
		
		public void actionPerformed(ActionEvent e){
			
			int id = view.getClientId();
			
			//Client c = ClientDao.findById(id);
			//Client k = ClientBLL.findClientById(id);
			//c = ClientBLL.findClientById(id);
			//if(c != null)
			System.out.println("Succes!");
		}
	}
	
	public class InsertClientTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			
			String s1 = view.getClientName();
			int i1 = view.getClientPhone();
			String s2 = view.getClientEmail();
			String s3 = view.getClientAddress();
			Client c = new Client(s1, i1, s2, s3);
			Client cInserare =  c;
			ClientBLL client = new ClientBLL();
			//System.out.println("Success!");
			if (cInserare != null)//{
				if (client.insertClient(cInserare) <= 0)
					System.out.println("Error!");
				else
					System.out.println("Success!");
			//}
		}
	}
	public class UpdateClientTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			int i = view.getClientId();
			String s1 = view.getClientName();
			int i1 = view.getClientPhone();
			String s2 = view.getClientEmail();
			String s3 = view.getClientAddress();
			Client c = new Client(s1, i1, s2, s3);
			Client cInserare =  c;
			ClientBLL client = new ClientBLL();
			//System.out.println("Success!");
			if (cInserare != null){
				if (client.updateClient(cInserare,i) != -1)
					System.out.println("Error!");
				else
					System.out.println("Success!");
			}
		}
	}
	
	public class DeleteClientTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			int i = view.getClientId();
			ClientBLL client = new ClientBLL();
				if (client.deleteClient(i) != -1)
					System.out.println("Error!");
				else
					System.out.println("Success!");
			}
		}
	
	public class ShowClientTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			view.createTable();
			System.out.println("Success!");
		}
	}
	
	public class FindProductTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			int id = view.getProductId();
			if(id != 0)
				System.out.println("Success!");
			else
				System.out.println("Error!");
		}
	}
	
	public class InsertProductTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			String s1 = view.getProductName();
			int i = view.getProductQuantity();
			String s2 = view.getProductType();
			Product p = new Product(s1,s2,i);
			Product insertProduct =  p;
			ProductBLL pp = new ProductBLL();
			//System.out.println("Success!");
			if (insertProduct != null)
				if (pp.insertProduct(insertProduct) <= 0)
					System.out.println("Error!");
				else
					System.out.println("Success!");
			
		}
	}
	
	public class UpdateProductTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			int i0 = view.getProductId();
			String s1 = view.getProductName();
			int i = view.getProductQuantity();
			String s2 = view.getProductType();
			Product p = new Product(s1,s2,i);
			Product upProduct =  p;
			ProductBLL pp = new ProductBLL();
			//System.out.println("Success!");
			if (upProduct != null){
				if (pp.updateProduct(upProduct,i0) != -1)
					System.out.println("Error!");
				else
					System.out.println("Success!");
			}
		}
	}
	
	public class DeleteProductTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			int i = view.getProductId();
			ProductBLL p = new ProductBLL();
				if (p.deleteProduct(i) != -1)
					System.out.println("Error!");
				else
					System.out.println("Success!");
		}
	}
	
	public class ShowProductTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			//view.createTable();
			System.out.println("Success!");
		}
	}
	
	public class CreateOrderTListener implements ActionListener{
		
		public void actionPerformed(ActionEvent e){
			
			int id1 = view.getProductId2();
			int id2 = view.getClientId2();
			int i = view.getProductQuantity2();
			Order o = new Order(id2,id1,i);
			Order insertOrder =  o;
			OrderBLL oo = new OrderBLL();
			//System.out.println("Success!");
			if (insertOrder != null)
				//if (pp.insertProduct(insertProduct) <= 0)
					System.out.println("Success!");
				else
					System.out.println("Error!");
			
			///////////////////scriere in fisier
				PrintWriter out = null;
				try {
					out = new PrintWriter(new BufferedWriter(new FileWriter("fisier.txt", true)));
					out.println("orderID  = " + 1 + "; clientID = " + id2 + "; productID  = " + id1 + "; Quantity = " + i + "; Price = " + i*5); 
					System.out.println("Success!");
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				out.close();
				
			}
		}
	
}
