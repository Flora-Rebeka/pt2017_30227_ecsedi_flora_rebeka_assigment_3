package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import Model.Order;
import bll.validators.QuantityValidator;
import bll.validators.Validator;
import dao.OrderDao;

public class OrderBLL {

	private List<Validator<Order>> validators;

	public OrderBLL() {
		validators = new ArrayList<Validator<Order>>();
		validators.add(new QuantityValidator());
	}

	public Order findOrderById(int id) {
		Order o = OrderDao.findById(id);
		if (o == null) {
			throw new NoSuchElementException("The order with id =" + id + " was not found!");
		}
		return o;
	}

	public int createOrder(Order o) {
		for (Validator<Order> v : validators) {
			v.validate(o);
		}
		return OrderDao.create(o);
	}
	
	
}
