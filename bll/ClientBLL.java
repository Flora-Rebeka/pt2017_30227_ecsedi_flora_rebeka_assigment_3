package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.EmailValidator;
import bll.validators.PhoneValidator;
import bll.validators.Validator;
import dao.ClientDao;
import Model.Client;


public class ClientBLL {

	private List<Validator<Client>> validators;

	public ClientBLL() {
		validators = new ArrayList<Validator<Client>>();
		//validators.add(new EmailValidator());
		//validators.add(new PhoneValidator());
	}

	public Client findClientById(int id) {
		Client cl = ClientDao.findById(id);
		if (cl == null) {
			throw new NoSuchElementException("The client with id =" + id + " was not found!");
		}
		return cl;
	}

	public int insertClient(Client client) {
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		return ClientDao.insert(client);
	}
	
	public int updateClient(Client client, int id) {
		if (ClientDao.findById(id) == null){
			return -1;
		}
		for (Validator<Client> v : validators) {
			v.validate(client);
		}
		return ClientDao.update(client,id);
	}
	
	public int deleteClient(int id){
		if (ClientDao.findById(id) == null){
			return -1;
		}
		else{
			return ClientDao.delete(id);
		}
	}
}