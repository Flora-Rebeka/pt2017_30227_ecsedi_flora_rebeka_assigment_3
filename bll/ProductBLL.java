package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import bll.validators.QValidator;
import bll.validators.Validator;
import dao.ClientDao;
import dao.ProductDao;
import Model.Product;

public class ProductBLL {

	private List<Validator<Product>> validators;
	
	public ProductBLL() {
		validators = new ArrayList<Validator<Product>>();
		//validators.add(new QValidator());
	}
	
	public Product findProductById(int id) {
		Product p = ProductDao.findById(id);
		if (p == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		return p;
	}
	
	public int insertProduct(Product product) {
		for (Validator<Product> v : validators) {
			v.validate(product);
		}
		return ProductDao.insert(product);
	}
	
	public int updateProduct(Product product, int id) {
		if (ClientDao.findById(id) == null){
			return -1;
		}
		for (Validator<Product> v : validators) {
			v.validate(product);
		}
		return ProductDao.update(id,product);
	}
	
	public int deleteProduct(int id){
		if (ProductDao.findById(id) == null){
			return -1;
		}
		else{
			return ProductDao.delete(id);
		}
	}
}
