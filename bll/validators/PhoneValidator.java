package bll.validators;

import java.util.regex.Pattern;

import Model.Client;

public class PhoneValidator implements Validator<Client> {
	private static final String PHONE_PATTERN = "\\d{10}";
	
	public void validate(Client t) {
		Pattern pattern = Pattern.compile(PHONE_PATTERN);
		
		//String phone = Integer.toString(t.getPhone());
		
		if (!pattern.matcher(Integer.toString(t.getPhone())).matches()) {
			throw new IllegalArgumentException("Phone is not a valid number!");
		}	
	}

	
}
