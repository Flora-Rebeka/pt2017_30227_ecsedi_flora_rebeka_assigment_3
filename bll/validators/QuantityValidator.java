package bll.validators;

import java.util.NoSuchElementException;

import Model.Order;
//import Model.Product;
import Model.Product;
import dao.ProductDao;

public class QuantityValidator implements Validator<Order>{
	
	
	public void validate(Order t) {
		
		if (t.getQuantity() <= 0) {
			throw new IllegalArgumentException("Quantity is not a valid quantity");
		}
		
		int id = t.getProductId();
		Product p = ProductDao.findById(id);
		
		if (p == null) {
			throw new NoSuchElementException("The product with id =" + id + " was not found!");
		}
		else if(p.getQuantity() < t.getQuantity()){
			throw new IllegalArgumentException("Quantity is not a valid quantity");
		}
	}

}
