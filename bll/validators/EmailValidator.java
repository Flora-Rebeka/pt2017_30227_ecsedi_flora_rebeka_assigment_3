package bll.validators;

import java.util.regex.Pattern;

import Model.Client;

public class EmailValidator implements Validator<Client> {
	private static final String EMAIL_PATTERN = "([a-zA-Z0-9._]{1,25}\\@[a-zA-Z0-9_]{3,25}.[a-zA-Z0-9_]{1,5})";

	public void validate(Client t) {
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		if (!pattern.matcher(t.getEmail()).matches()) {
			throw new IllegalArgumentException("Email is not a valid email!");
		}
	}

}