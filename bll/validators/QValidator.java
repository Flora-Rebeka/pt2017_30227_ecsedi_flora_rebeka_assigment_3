package bll.validators;


import Model.Product;

public class QValidator implements Validator<Product>{
	
	
	public void validate(Product t) {
		
		if (t.getQuantity() < 0) {
			throw new IllegalArgumentException("Quantity is not a valid quantity");
		}
		
	}

}
