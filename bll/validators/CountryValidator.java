package bll.validators;

import Model.Store;

public class CountryValidator implements Validator<Store>{

	
	public void validate(Store t) {
		
		if (t.getCountry() != "Romania" || t.getCountry() != "Hungaria" || t.getCountry() != "Germany") {
			throw new IllegalArgumentException("Country is not a valid country");
		}
		
	}

}
