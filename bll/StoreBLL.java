package bll;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;


import dao.StoreDao;
import Model.Store;
import bll.validators.CountryValidator;
import bll.validators.Validator;

public class StoreBLL {

	private List<Validator<Store>> validators;
	
	public StoreBLL() {
		validators = new ArrayList<Validator<Store>>();
		validators.add(new CountryValidator());
	}
	
	public Store findStoreById(int id) {
		Store st = StoreDao.findById(id);
		if (st == null) {
			throw new NoSuchElementException("The store with id =" + id + " was not found!");
		}
		return st;
	}

	public int insertStore(Store store) {
		for (Validator<Store> v : validators) {
			v.validate(store);
		}
		return StoreDao.insert(store);
	}
}
