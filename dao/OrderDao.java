package dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

import connection.ConnectionFactory;
import Model.Order;


public class OrderDao {

	protected static final Logger LOGGER = Logger.getLogger(OrderDao.class.getName());
	private static final String insertStatementString = "INSERT INTO order (clientId,productId,quantity)"
			+ " VALUES (?,?,?)";
	private final static String findStatementString = "SELECT * FROM order where id = ?";
	//private static final String updateStatementString = "UPDATE order SET clientId = ?, productId = ?, quantity = ? WHERE ID = ?";
	//private static final String deleteStatementString  = "DELETE FROM order where id = ?";
	
	public static Order findById(int orderId) {
		Order toReturn = null;

		Connection dbConnection = ConnectionFactory.getConnection();
		PreparedStatement findStatement = null;
		ResultSet rs = null;
		try {
			findStatement = dbConnection.prepareStatement(findStatementString);
			findStatement.setLong(1, orderId);
			rs = findStatement.executeQuery();
			rs.next();

			int clientId = rs.getInt("clientId");
			int productId = rs.getInt("productId");
			int quantity = rs.getInt("quantity");
			
			toReturn = new Order(clientId, productId, quantity);
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING,"OrderDAO:findById " + e.getMessage());
		} finally {
			ConnectionFactory.close(rs);
			ConnectionFactory.close(findStatement);
			ConnectionFactory.close(dbConnection);
		}
		return toReturn;
	}

	public static int create(Order order) {
		Connection dbConnection = ConnectionFactory.getConnection();

		PreparedStatement insertStatement = null;
		int insertedId = -1;
		try {
			insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
			//insertStatement.setInt(1, order.getOrderId());
			insertStatement.setInt(1, order.getClientId());
			insertStatement.setInt(2, order.getProductId());
			insertStatement.setInt(3, order.getQuantity());
			insertStatement.executeUpdate();

			ResultSet rs = insertStatement.getGeneratedKeys();
			if (rs.next()) {
				insertedId = rs.getInt(1);
			}
		} catch (SQLException e) {
			LOGGER.log(Level.WARNING, "OrderDAO:insert " + e.getMessage());
		} finally {
			ConnectionFactory.close(insertStatement);
			ConnectionFactory.close(dbConnection);
		}
		return insertedId;
	}
}
