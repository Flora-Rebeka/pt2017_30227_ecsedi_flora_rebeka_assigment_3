package start;

import presentation.View;
import presentation.SecondFrame;

import java.awt.List;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Logger;

import Model.Client;
import Model.Product;
import bll.ClientBLL;
import bll.ProductBLL;
import dao.ClientDao;
import presentation.Controler;

public class Start {
	
	protected static final Logger LOGGER = Logger.getLogger(ClientDao.class.getName());
	
	public static void main(String[] args){
		View v = new View();
		Controler c = new Controler(v);
		
}
}
